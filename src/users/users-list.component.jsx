import React from 'react'

export const UsersList = ({ users, onSelect, selected }) => 
<div className="list-group">
    {users.map(user => {
        let active = ( selected && selected.id == user.id) ? 'active' : '';

        return <div key={user.id}
            onClick={e => onSelect(user)}
            className={`list-group-item ${active}`}>
            {user.name}
        </div>
    })}
</div>


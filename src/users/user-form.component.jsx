import React from 'react'

export class UserForm extends React.Component {

    static propTypes = {

    }

    constructor(props) {
        super(props)
        this.state = {
            user: props.user
        }
    }

    componentWillReceiveProps(newProps = {}) {
        this.setState({
            user: newProps.user
        })
    }

    update = (event) => {
        let input = event.target
        let field = input.name

        let user = this.state.user

        user[field] = input.value

        this.setState({
            user
        })
    }

    submit = event => {
        event.preventDefault()

        this.props.onSave(this.state.user)
    }

    render() {
        let user = this.state.user;

        return <div>
            {user && <form onSubmit={this.submit}>
                <div className="form-group">
                    <label>Username</label>
                    <input className="form-control"
                        name="username" onChange={this.update} value={user.username || ''} />
                </div>
                <div className="form-group">
                    <label>Name</label>
                    <input className="form-control"
                        name="name" onChange={this.update} value={user.name || ''} />
                </div>
                <div className="form-group">
                    <label>E-mail</label>
                    <input className="form-control"
                        name="email" onChange={this.update} value={user.email || ''} />
                </div>
                <div className="form-group">
                    <button>Save</button>
                </div>
            </form>}
        </div>
    }
}

window.INC = { type: 'INC' }
window.DEC = { type: 'DEC' }


export const counter = (state = 0, action) => {

    switch (action.type) {
        case 'INC': return state + 1
        case 'DEC': return state - 1
    }
    return state
}

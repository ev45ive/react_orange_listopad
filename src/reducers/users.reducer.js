import axios from 'axios'

export const loadUsers = (users) => ({
    type:'USERS_LOAD',
    payload: users
})

export const fetchUsers = (dispatch) => {
    return axios.get('http://localhost:3000/users/')
    .then(response => dispatch(loadUsers(response.data)))
}

export const saveUser = (dispatch) => user => {
    let request 
    if(user.id){
        // update
        request = axios.put('http://localhost:3000/users/' +user.id, user)
    }else{
        // create
        request = axios.post('http://localhost:3000/users/', user)
    }
    return request.then((response) => {
        // this.selectUser(response.data)
        fetchUsers(dispatch)
        return response.data
    })
}

export const initialUsers = {
    list: [
        {
            "id": 1,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz"
        },
        {

            "id": 2,
            "name": "Ervin Howell",
            "username": "Antonette",
            "email": "Shanna@melissa.tv"
        }
    ]
}

export const users = (state = initialUsers, action) => {
    switch (action.type) {
        case 'USERS_LOAD':
            return {
                list: action.payload
            }
        default:
            return state;
    }
};

export const todoAdd = todo => ({
    type: 'TODO_ADD',
    payload: { todo }
})

export const todoFromTitle = title => ({
    type: 'TODO_ADD',
    payload: { todo:{
        id: Date.now(),
        title,
        completed:false
    }}
})

export const todoToggle = todo => ({
    type: 'TODO_UPDATE',
    payload: {
        todo: Object.assign({},todo,{
            completed: !todo.completed
        })
    }
})

export const todoRemove = id => ({
    type: 'TODO_REMOVE',
    payload: id
})

export const initialTodos = {
    list: []
}


export const todos = (state = initialTodos, action) => {

    switch (action.type) {
        case 'TODO_ADD': return {
            list: [...state.list, action.payload.todo]
        }
        case 'TODO_UPDATE': return {
            list: state.list.map(
                todo => todo.id == action.payload.todo.id?
                action.payload.todo : todo
            )
        }
        case 'TODO_REMOVE': return {
            list: state.list.filter(
                todo => todo.id != action.payload
            )
        }
    }
    return state
}

var path = require('path')
var ExtractPlugin = require('extract-text-webpack-plugin')
var HtmlPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        main:'./src/main.jsx',
        // admin:  './src/admin.js'
    },
    output: {
        path: path.resolve('dist'),
        filename: '[name].js'
    },
    resolve:{
        extensions:['.jsx','.js']
    },
    devtool: 'sourcemap',
    plugins: [
        new ExtractPlugin('styles.css'),
        new HtmlPlugin({
            chunks: 'main',
            template: './src/index.html'
        }),
        // new HtmlPlugin({
        //     chunks: 'admin',
        //     template: './src/admin.html'
        // })
    ],
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: 'babel-loader'
            },
            {
                test: /\.css?$/,
                use: ExtractPlugin.extract('css-loader')
            }
        ]
    },
    devServer:{
        historyApiFallback:true,
        proxy:{
            // 
        }
    }
}
inc = (amount = 1) => ({
	type:'INC', payload:amount
}); 
dec = (amount = 1) => ({
	type:'DEC', payload:amount
});

[inc(),inc(2),inc(2),dec(3),inc()].reduce( (state, action) => {

	switch(action.type){
        case 'INC': return { value: state.value + action.payload }
        case 'DEC': return { value: state.value - action.payload }
    }
	return state
},{
	value: 0
})

/////////////

inc = (amount = 1) => ({
	type:'INC', payload:amount
}); 
dec = (amount = 1) => ({
	type:'DEC', payload:amount
});

counterReducer = (state = 0, action) => {
	switch(action.type){
        case 'INC': return state + action.payload 
        case 'DEC': return state - action.payload 
    }
	return state
}

[inc(),inc(2),inc(2),dec(3),inc()].reduce( (state, action) => {

	return Object.assign({},state,{
		counter: counterReducer(state.counter, action),
		counter2: counterReducer(state.counter, action)
    })
},{
	counter: 0,
	counter2: 0
})